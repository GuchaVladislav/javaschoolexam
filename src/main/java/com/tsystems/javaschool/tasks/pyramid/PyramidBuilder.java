package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if(Math.sqrt(inputNumbers.size()*4*2+1)%1!=0) throw new CannotBuildPyramidException();  //verification of the discriminant
        int heightMat=(int) (Math.sqrt(inputNumbers.size()*4*2+1)-1)/2;     //finding the height of the pyramid
        try {
            inputNumbers.sort(Comparator.naturalOrder());
        }catch (java.lang.NullPointerException e){
            throw new CannotBuildPyramidException();
        }

        int[][] result;
        result = new int [heightMat][2*heightMat-1];

        for (int i=heightMat-1,element=inputNumbers.size()-1,barrier=0;i>=0;i--,barrier++){
            for(int j=2*heightMat-2;j>=barrier;j--){
                if((j+barrier)%2==0 && j>=barrier && j<2*heightMat-1-barrier) {
                    result[i][j]=inputNumbers.get(element);
                    element--;
                }
                else result[i][j]=0;
            }
        }
        return result;
    }

}
