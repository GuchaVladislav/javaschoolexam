package com.tsystems.javaschool.tasks.calculator;

import java.util.Locale;
import java.util.Stack;
import java.util.regex.*;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if(statement==null || statement=="") return null;
        statement=statement.concat("$");
        Pattern numer = Pattern.compile("^\\d+(\\.\\d+)?");         //regular expression of a number
        Stack<String> operators = new Stack<>();                    //stack of operators
        Stack<Double> numbers = new Stack<>();                      //stack of numbers
        operators.push("$");
        do {
            while (statement.charAt(0) == '(') {                    //catching parentheses at the beginning of the expression
                operators.push("(");
                statement=statement.replaceFirst("\\(","");
            }
            String temp=statement.split("[$()*+/-]")[0];      //get the first number from the string
            if(!numer.matcher(temp).matches()) return null;         //if this is not a valid number then an error
            numbers.push(new Double(temp));                         //add a number to the stack
            statement=statement.replaceFirst("[\\d\\\\.]+","");     // and remove it from the input line
            if (statement.charAt(0)!='+' && statement.charAt(0)!='-' && statement.charAt(0)!='$'&&
                    statement.charAt(0)!='/'&& statement.charAt(0)!='*'&& statement.charAt(0)!=')') return null;    //wait for the operator, if there is not an error
            while(true){                                                        //we are doing all available mathematical actions in this step
                if((statement.charAt(0)=='*' || statement.charAt(0)=='/')
                        &&(operators.peek().charAt(0)=='/' ||operators.peek().charAt(0)=='*')){
                    Double b=numbers.pop();
                    Double a=numbers.pop();
                    numbers.push(operation (operators.pop(),a,b));
                    continue;
                }
                if((statement.charAt(0)=='+' || statement.charAt(0)=='-'|| statement.charAt(0)=='$'|| statement.charAt(0)==')')
                        &&(operators.peek().charAt(0)=='-' || operators.peek().charAt(0)=='*'
                        ||operators.peek().charAt(0)=='/' || operators.peek().charAt(0)=='+')){
                    if(operators.peek().charAt(0)=='/' && numbers.peek()==0.0) return null;
                    Double b=numbers.pop();
                    Double a=numbers.pop();
                    numbers.push(operation (operators.pop(),a,b));
                    continue;
                }
                if(statement.charAt(0)==')' &&operators.peek().charAt(0)=='('){
                    operators.pop();
                    statement=statement.replaceFirst("[)]","");
                    continue;
                }
                break;
            }
            if(statement.charAt(0)=='$') {                  //if there is an end-of-line marker
                double result=numbers.pop();
                if(numbers.isEmpty() && operators.peek().charAt(0)=='$'){   //and the number stack has one number, and the stack of operators is having only one value
                    int res=(int) result;
                    if(res==result) return new Integer(res).toString();     //if number is an integer output a its
                    result = result * 10000;
                    int i = (int) Math.round(result);
                    result = (double)i / 10000;
                    return new Double(result).toString();           //if number is an double output a its
                }
                else return null;
            }
            operators.push(String.valueOf(statement.charAt(0)));         //add a operator to the stack
            statement=statement.replaceFirst("[+\\-*/]","");
        }while(!operators.isEmpty() || !numbers.isEmpty());
        return "";
    }
    public Double operation(String operator, Double a,Double b) {
        switch (operator) {
            case "+": return a + b;
            case "-": return a - b;
            case "*": return a * b;
            case "/": return a / b;
        }
        return new Double(0);
    }
}
